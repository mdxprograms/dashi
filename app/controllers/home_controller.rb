class HomeController < ApplicationController
  def index
    @title = 'Home'
    @todos = Todo.all.order('created_at desc')
  end
end
