class AddColorToTodoStatuses < ActiveRecord::Migration
  def change
    add_column :todo_statuses, :color, :string
  end
end
