class CreateTodos < ActiveRecord::Migration
  def change
    create_table :todos do |t|
      t.string :title
      t.text :description
      t.datetime :started_at
      t.datetime :completed_at
      t.references :todo_status, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
