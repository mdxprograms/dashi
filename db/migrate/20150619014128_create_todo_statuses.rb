class CreateTodoStatuses < ActiveRecord::Migration
  def change
    create_table :todo_statuses do |t|
      t.string :status

      t.timestamps null: false
    end
  end
end
